#!/bin/bash

# Остановка всех служб
systemctl stop openvpn-server@server
systemctl stop stunnel4
systemctl stop wg-quick@wg0
systemctl stop zabbix-agent

# Удаление пакетов openvpn, stunnel4, wireguard, zabbix-agent
apt-get purge -y openvpn stunnel4 wireguard zabbix-agent

# Удаление конфигурационных файлов и директорий
rm -rf /etc/openvpn
rm -rf /etc/stunnel
rm -rf /etc/wireguard
rm -rf /etc/zabbix

# Удаление пользователей пакетов
deluser --remove-home stunnel4
deluser --remove-home zabbix

# Удаление истории команд
history -c
history -w

# Удаление логинов
find /var/log -type f -name "*log" -delete

# Очистка логов системы
journalctl --rotate
journalctl --vacuum-time=1s

# Очистка логов secure, messages и syslog
truncate /var/log/auth.log --size 0
truncate /var/log/syslog --size 0
truncate /var/log/messages --size 0

# Определение переменных для корректного сброса iptables
WAN_IP=$(ip -4 addr | sed -ne 's|^.* inet \([^/]*\)/.* scope global.*$|\1|p' | awk '{print $1}' | head -1)
WAN=$(ip -4 route ls | grep default | grep -Po '(?<=dev )(\S+)' | head -1)
WAN_PORT=$(grep -E "^Port\s+" /etc/ssh/sshd_config | awk '{print $2}')

# Запись и применение чистой конфигурации iptables
echo "#!/bin/bash

export WAN=$WAN
export WAN_IP=$WAN_IP
export WAN_PORT=$WAN_PORT

iptables -F
iptables -F -t nat
iptables -F -t mangle
iptables -X
iptables -t nat -X
iptables -t mangle -X

iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

iptables -A INPUT -p icmp --icmp-type echo-reply -j ACCEPT
iptables -A INPUT -p icmp --icmp-type destination-unreachable -j ACCEPT
iptables -A INPUT -p icmp --icmp-type time-exceeded -j ACCEPT
iptables -A INPUT -p icmp --icmp-type echo-request -j ACCEPT

iptables -A OUTPUT -o $WAN -j ACCEPT

iptables -A INPUT -p all -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -p all -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -p all -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT

iptables -I FORWARD -p tcp --tcp-flags SYN,RST SYN -j TCPMSS --clamp-mss-to-pmtu

iptables -A INPUT -m state --state INVALID -j DROP
iptables -A FORWARD -m state --state INVALID -j DROP

iptables -A INPUT -p tcp ! --syn -m state --state NEW -j DROP
iptables -A OUTPUT -p tcp ! --syn -m state --state NEW -j DROP

iptables -A INPUT -i $WAN -p tcp --dport $WAN_PORT -j ACCEPT

/sbin/iptables-save > /etc/iptables/rules.v4" > /etc/iptables.sh
bash /etc/iptables.sh

# Перезагрузка сервера
init 6
