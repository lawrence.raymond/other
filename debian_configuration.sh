#!/bin/bash

function isRoot() {
	if [ "$EUID" -ne 0 ]; then
		return 1
	fi
}

function initialCheck() {
	if ! isRoot; then
		echo ""
		echo "Please run this script as root!"
		echo ""
		exit 1
	fi
}

function installQuestions() {
	clear
	newClientName=$(</dev/urandom tr -dc a-z | head -c 8)
	read -rp "Enter a new username: " -e -i "${newClientName}" newClientName

	newClientPassword=$(</dev/urandom tr -dc a-z-A-Z-0-9 | head -c 16)
	read -rp "Enter a new password for the user: " -e -i "${newClientPassword}" newClientPassword
}


function systemSetup() {
	installQuestions

	# VPN configuration
	cp client.conf user.pass /etc/openvpn/client/
	systemctl enable openvpn-client@client

	# Setting up a new username and password
	sudo usermod -l $newClientName user
	chfn -f $newClientName $newClientName
	echo $newClientName:$newClientPassword | chpasswd

	# SSH configuration
	mkdir /home/user/.ssh
	echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDrjG8zDdgkLucFh/c10bQxMBfsxnRt9+U5zToymu+bmPhCzIKXk5wxR05+4+eEzvOgRJgGSrLXhVd1uuQeNEcvfsIlcWSnnmB/oKr2E+pkPA6eisXC6MFzAKnCU1oLC7DVOmd0RAE2wTKLdBcfuoTFZ6ritNQG4S5Q3NuLIESkdAuIC6byhRSpguD66EcRmicVEsYZRZBH+cNdwWFVB4OsUS4iS3wwoU5AIOuVQLarqYSacwWC3JR4Z2WABi0gkwKk7Oq/cq6XCg1Bq+QNgfTWFyLUGPuLzKOyf3DkYabdE6jIX/JsgeSnha/BxKzIXAvowU7vzUAuFHnbaRiC6gX518XHZM9jzWe8y6EEc29dKyraWAJoY7LtHpW73enKdV8rnJF4B8mcmmNtXI28WTtiPImZ+brMrK9T0Q3MDF06ku+wMRLKZwA203K17Tm+/IyVSmzDlOPmfgF+gKRL751JoiZF0QWSq0Ymwrw1SXtCTRCzh/mzbQh0EkRqti/Lau8= root@srv1670413697.hosttoname.com" >> /home/user/.ssh/authorized_keys

	# Disable SSH login from root
	sed -i 's/#PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
	sed -i 's/PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
	sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin no/g' /etc/ssh/sshd_config

	# Change SSH port to 25431
	sed -i 's/#Port 22/Port 25431/g' /etc/ssh/sshd_config

	# Autologin
	mkdir /etc/lightdm/lightdm.conf.d
	echo -e "[SeatDefaults]\nautologin-user=$newClientName\nautologin-user-timeout=0" >> /etc/lightdm/lightdm.conf.d/autologin.conf

	# sudo
	usermod -aG sudo $newClientName

	clear
	echo ""
	echo "System setup completed successfully!"
	echo "New login details for user: $newClientName:$newClientPassword"
	echo ""
	echo "You need to reboot the system."
	read -p "Press ENTER to reboot the system and complete the setup."
	sudo shutdown -r now
}

initialCheck
systemSetup
