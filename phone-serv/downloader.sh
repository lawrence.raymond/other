#!/bin/bash

Device="VPN_USER_NAME"                                           # Клиент

sleep_time="900"                                            # время между циклами обработки (в секундах).   900сек = 15мин
max_mistakes=5                                              # количесво ошибок подряд, при закачке файла.(цикл завершается и начнётся заново через 15 мин.)

PAPKA="/home/path/mounted_dir"                                  # папка с индикатором готовности сотрудника
indicator="indicator.txt"                                   # файл-индикатор готовности сотрудника
Download_dir="/home/path/downloads"                         # место сохранения файлов на компе
user_files_dir="/sdcard/folder_name"           # место сохранения файлов в телефоне
test_conection_log="/etc/openvpn/status-users-udp.log" # файл проверки подключено ли устройство к корпоративной сети

#######################################################################
 





restart() {

if [ -f "$PAPKA/$indicator" ];then
	test_conection=$(cat "$test_conection_log" | awk -F"," '{print $2}' | grep -w "$Device")
	if [ "$test_conection" ];then
		adb connect 10.8.0.10:25620
		sleep 2
#		files_on_phone=$(adb shell ls -l "$user_files_dir")
		files_on_phone=$(adb shell ls -l "$user_files_dir" | grep -vi "total" | grep -v ^"d")
		echo; echo; echo "       Найдены Файлы на телефоне:"; echo
		echo -e "$files_on_phone"	;echo
			if [ "$files_on_phone" ];then
				IFS=$'\n'
				for line in $files_on_phone
				do				
					mistake=0
					name_file=$(echo "$line" | awk '{print $8}')
					size_on_phone=$(echo "$line" | awk '{print $5}')
#					echo 'ИМЯ ФАЙЛА'
#					echo $name_file
#					echo 'РАЗМЕР ФАЙЛА'
#					echo $size_on_phone
					test_downloaded(){
				
						if [ $mistake -ge $max_mistakes ];then
							echo "              ERROR: ${name_file} - Can NOT download file $max_mistakes times" 
							break
							#	continue
						fi
					
					exist_on_comp=$(ls -l "$Download_dir" | awk '{print $9}' | grep -w "$name_file")
					if [ -f "${Download_dir}/${name_file}" ];then
						size_on_comp=$(ls -l "${Download_dir}/${name_file}" | awk '{print $5}')			
					fi					
					
					if [[ "$exist_on_comp" && "$size_on_phone" -eq "$size_on_comp" ]];then
					
						 adb shell rm -f "${user_files_dir}/${name_file}"
						echo "File exist   Delete: $name_file"
					else
					    
						adb pull "${user_files_dir}/${name_file}" "${Download_dir}/${name_file}"
						echo "File    Downloading: $name_file"
						mistake=$(($mistake+1))
						test_downloaded
					fi
					}
					test_downloaded
				done
			else
				echo "  No Files on Phone!"
			fi
	else
		echo "          No Person: \"$Device\" in NetWork"		
	fi

else
	echo " Not indicator File: $PAPKA/$indicator"
fi



echo; echo
echo "            Done!"
echo " Whait for Re-Start in $sleep_time cek....."
echo; echo
sleep "$sleep_time"
restart
}
restart
exit 0


