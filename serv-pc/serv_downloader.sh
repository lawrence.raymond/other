#!/bin/bash

#####################################################################

time_update=30

SERVER_PORT=22
user="user_server"
server="ip_server"
ssh_key="/home/path/id_rsa"


server_dir="/home/path/photo"
beckup_dir="/home/path/dir_name"
filter=".png"
#####################################################################


RED=$(tput setaf 1)
GREEN=$(tput setaf 2)
YELLOW=$(tput setaf 3)
NC=$(tput sgr0) #  Stop color

beckup_action(){

echo
echo $GREEN"Scaning...."$NC
files_on_server=$(ssh -p$SERVER_PORT $user@$server "ls -l $server_dir | grep $filter")

IFS=$'\n'
echo "======================="
echo $GREEN"--- Files on Serve: ---"$NC
echo -e "$files_on_server"
echo "======================="
echo
echo "======================="
echo $GREEN"  Compering sizes..."$NC




for file_line in $files_on_server
do


          file=$(echo "$file_line" | awk '{print $8}')
size_on_server=$(echo -e "$file_line" | awk '{print $5}')
	 test_file=$(ls -l "$beckup_dir" | grep -w "$file")

	 
if [ "$test_file" ];then
	size_on_beckup=$(ls -l "$beckup_dir" | grep -w "$file"| awk '{print $5}' | head -1)
	if [ "$size_on_server" -ne "$size_on_beckup" ];then
		echo $RED"File exist (but not the same size).Replace -----> $file"$NC

		list_new_files="$list_new_files,$server_dir/$file"
	else 
		echo "File exist: $file"
	fi	
else
		echo $RED"File NOT exist.  to BeckUp  -----> $file"$NC		
		list_new_files="$list_new_files,$server_dir/$file"
fi
done
list_new_files=$(echo "$list_new_files" | sed 's/^,//g')


echo "======================="
echo
echo "======================="
echo $GREEN"--- List for trancfer: "$NC
echo "|$list_new_files|"
echo "======================="
echo

#list=$(echo -e "$list_new_files" | tr '\n' ' ' | grep '.')
test_list=$(echo -e "$list_new_files" | grep '.' | sed 's/ //g')

if [ "$test_list" ];then
	echo "======================="
	echo $GREEN"  Transfering....."$NC
    
	scp -P $SERVER_PORT $user@$server:{${list_new_files}} $beckup_dir
	echo $NC
else
	echo $YELLOW"---------------every thing up to date-----------------"$NC
fi
echo $GREEN"-----pauza $time_update cek ........"$NC

unset list_new_files
unset test_list
sleep $time_update

beckup_action
}

beckup_action

exit 0
