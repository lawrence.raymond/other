#!/bin/bash

# Get the list of files
files=$(ls | grep ".png")

# Loop through each file
for file in $files
do
	# Get the file name without the extension
	filename=$(echo $file | cut -f 1 -d '-')

	# Move the file into the folder
	if [[ -d $filename ]]; then

		# Check if the file name is repeated
                if test -f "$filename/$file"; then
			# Get the number of files with the same name
			count=$(ls $filename | grep -c "$file")

			# Rename the file by adding a number of duplicate
			duplicateFilename=$(echo $file | cut -f 1 -d '.')
			mv $file $filename/$duplicateFilename"-"$count".png"
		else
			mv $file $filename
		fi
	else
		# Create a folder with the same name as the file
		mkdir $filename
		mv $file $filename
	fi
done
