#!/bin/bash

addresses=()
while read -r address; do
  addresses+=("$address")
done < "address.txt"

while read -r line; do
  if [[ "$line" =~ ^accept\ =\ ([0-9\.]+):([0-9]+)$ ]]; then
    random_address=${addresses[$RANDOM % ${#addresses[@]}]}
    new_line="accept = ${random_address}:${BASH_REMATCH[2]}"
    echo "${line//$BASH_REMATCH/$new_line}"
  else
    echo "$line"
  fi
done < "stunnel.conf" > "stunnel.conf.new"

mv "stunnel.conf.new" "stunnel.conf"

systemctl restart stunnel4
